const express = require('express');




//Grupo 3 
const { obtenerStockController,modificarStockController} = require('./src/Grupo3/controller/StockController');
const { obtenerPrecioController,ModificarPrecioController} = require('./src/Grupo3/controller/PrecioController');
const { obtenerEstadoController,ModificarEstadoController} = require('./src/Grupo3/controller/EstadoController');
const { modificarProductosAlmacenController,obtenerProductosAlmacenController} = require('./src/Grupo3/controller/ProductosAlmacenController');
const { ModificarProveedorController,ObtenerProveedorController} = require('./src/Grupo3/controller/ProveedorController');
const { ModificarCategoriaController,obtenerCategoriaController} = require('./src/Grupo3/controller/CategoriaController');
const { ModificarDetalleCompraController,obtenerDetalleCompraController} = require('./src/Grupo3/controller/DetalleCompraController');
const { ModificarProductoProveedorController,obtenerProductoProveedorController} = require('./src/Grupo3/controller/ProductoProveedorController');
const { modificarVentasController,obtenerVentasController} = require('./src/Grupo3/controller/VentasController');
//a



const { obtenerCargosController, modificarCargoController} = require('./src/controllers/CargoController');
const { obtenerEmpleadosController, modificarEmpleadoController} = require('./src/controllers/EmpleadoController');
const { obtenerMesasController, modificarMesaController} = require('./src/controllers/MesaController');

// Anie Jacy Bejar Huachaca

const { obtenerProcesosController, modificarProcesoController} = require('./src/controllers/ProcesoController');
const { obtenerReservasController, modificarReservaController} = require('./src/controllers/ReservaController');

// Castillo Mejia Rogers (Grupo 2)
const { obtenerPlatosController, modificarPlatosController } = require('./src/BackEndG2/src/controllers/PlatoController');
const { obtenerCategoriaPlatosController, modificarCategoriaPlatosController } = require('./src/BackEndG2/src/controllers/CategoriaPlatoController');

// Huaycha Cruz Lizardo Adan (Grupo 2)
const { obtenerDetalleComprobanteController, modificarComprobanteController } = require('./src/BackEndG2/src/controllers/DetalleComprobanteController'); 

// Ricanqui Alvaro GRUPO 4
const { obtenerPersonasController,modificarPersonaController} = require('./src/Grupo4/controllers/PersonaController');
const { obtenerDeliveryController,modificarDeliveryController} = require('./src/Grupo4/controllers/DeliveryController');
const { obtenerPedidosController,modificarPedidoController} = require('./src/Grupo4/controllers/PedidoController');
const { obtenerTipoMesaController,modificarTipoMesaController} = require('./src/Grupo4/controllers/TipoMesaController');
const { obtenerTipoUsuarioController,modificarTipoUsuarioController} = require('./src/Grupo4/controllers/TipoUsuarioController');
const { obtenerUsuarioController,modificarUsuarioController} = require('./src/Grupo4/controllers/UsuarioController');



const app = express();
const PORT = process.env.PORT || 3000;
 
// Middleware para analizar JSON en solicitudes POST
app.use(express.json());
 
// Ruta para obtener todos los elementos de la tabla
// TB_Cargo
app.get('/obtenerCargos', obtenerCargosController);
app.post('/modificarCargos', modificarCargoController);
//TB_Empleado
app.get('/obtenerEmpleados', obtenerEmpleadosController);
app.post('/modificarEmpleado', modificarEmpleadoController);
//TB_Mesa
app.get('/obtenerMesas', obtenerMesasController);
app.post('/modificarMesa', modificarMesaController);


//Anie Jacy Bejar Huachaca

//TB_Proceso
app.get('/obtenerProcesos', obtenerProcesosController);
app.post('/modificarProceso', modificarProcesoController);
//TB_Reserva
app.get('/obtenerReservas', obtenerReservasController);
app.post('/modificarReserva', modificarReservaController);


//Grupo 3 
app.get('/obtenerStock', obtenerStockController);
app.post('/modificarStock',modificarStockController);
app.get('/obtenerPrecio', obtenerPrecioController);
app.post('/ModificarPrecio',ModificarPrecioController);
app.get('/obtenerEstado', obtenerEstadoController);
app.post('/ModificarEstado',ModificarEstadoController);
app.get('/obtenerProductosAlmacen', obtenerProductosAlmacenController);
app.post('/ModificarProductosAlmacen',modificarProductosAlmacenController);
app.get('/obtenerProveedor', ObtenerProveedorController);
app.post('/ModificarProveedor',ModificarProveedorController);
app.get('/obtenerCategoria', obtenerCategoriaController);
app.post('/ModificarCategoria',ModificarCategoriaController);
app.post('/ModificarDetalleCompra', ModificarDetalleCompraController);
app.get('/obtenerDetalleCompra',obtenerDetalleCompraController);
app.post('/ModificarProductoProveedor', ModificarProductoProveedorController);
app.get('/obtenerProductoProveedor',obtenerProductoProveedorController);
app.post('/ModificarVentas', modificarVentasController);
app.get('/obtenerVentas',obtenerVentasController);



// Castillo Mejia Rogers (Grupo 2)

app.get('/obtenerPlatos', obtenerPlatosController);
app.get('/obtenerCategoriaPlatos', obtenerCategoriaPlatosController);

app.post('/modificarPlatos', modificarPlatosController);
app.post('/modificarCategoriaPlatos', modificarCategoriaPlatosController)

// Huaycha Cruz Lizardo Adan (Grupo 2)

app.get('/obtenerDetalleComprobante', obtenerDetalleComprobanteController);
app.post('/modificarComprobante', modificarComprobanteController);


// Ricanqui Alvaro GRUPO 4

// Ruta Persona
app.get('/obtenerPersonas', obtenerPersonasController);
app.post('/modificarPersonas', modificarPersonaController);
// Ruta Delivery
app.get('/obtenerDelivery', obtenerDeliveryController);
app.post('/modificarDelivery', modificarDeliveryController);
// Ruta Pedido
app.get('/obtenerPedidos', obtenerPedidosController);
app.post('/modificarPedido', modificarPedidoController);
// Ruta TipoMesa
app.get('/obtenerTipoMesa', obtenerTipoMesaController);
app.post('/modificarTipoMesa', modificarTipoMesaController);
// Ruta TipoUsuario
app.get('/obtenerTipoUsuario', obtenerTipoUsuarioController);
app.post('/modificarTipoUsuario', modificarTipoUsuarioController);
// Ruta Usuario
app.get('/obtenerUsuario', obtenerUsuarioController);
app.post('/modificarUsuario', modificarUsuarioController);


// Iniciar el servidor
app.listen(PORT, () => {
  console.log(`Servidor API REST escuchando en el puerto ${PORT}`);
});

