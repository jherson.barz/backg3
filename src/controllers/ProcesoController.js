const {obtenerProcesos,modificarProceso} = require('../services/ProcesoService')
 
function obtenerProcesosController(req, res) {
    obtenerProcesos((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
 
function modificarProcesoController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarProceso(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  module.exports = {obtenerProcesosController,modificarProcesoController};
  