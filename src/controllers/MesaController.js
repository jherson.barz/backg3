const {obtenerMesas,modificarMesa} = require('../services/MesaService')
 
function obtenerMesasController(req, res) {
    obtenerMesas((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
 
function modificarMesaController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarMesa(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  module.exports = {obtenerMesasController,modificarMesaController};