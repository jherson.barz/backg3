const {obtenerCargos,modificarCargo} = require('../services/CargoService')
 
function obtenerCargosController(req, res) {
    obtenerCargos((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
 
function modificarCargoController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarCargo(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  module.exports = {obtenerCargosController,modificarCargoController};
 
