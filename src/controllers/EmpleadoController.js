const {obtenerEmpleados,modificarEmpleado} = require('../services/EmpleadoService')
 
function obtenerEmpleadosController(req, res) {
    obtenerEmpleados((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
 
function modificarEmpleadoController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarEmpleado(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  module.exports = {obtenerEmpleadosController,modificarEmpleadoController};