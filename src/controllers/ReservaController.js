const {obtenerReservas,modificarReserva} = require('../services/ReservaService')
 
function obtenerReservasController(req, res) {
    obtenerReservas((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
 
function modificarReservaController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarReserva(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  module.exports = {obtenerReservasController,modificarReservaController};