const { obtenerConexion } = require('../database copy/conexion_mysql');
 
function obtenerDetalleCompra(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarDetallesCompra()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 


function ModificarDetalleCompra(m, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL InsertarActualizarDetalleCompra(?, ?, ?,?,?, @parametro_salida)';
      connection.query(query, [m.idDetalle, m.idProducto, m.cantidad, m.precioUnitario,m.idproveedor], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }
  module.exports = { obtenerDetalleCompra,ModificarDetalleCompra};
