const { obtenerConexion } = require('../database copy/conexion_mysql');
 
function obtenerProductosAlmacen(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarProductosAlmacen()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 


function ModificarProductosAlmacen(m, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL insertarActualizacionProductosAlmacen(?, ?, ?,?,?, @parametro_salida)';
      connection.query(query, [m.idProducto, m.nombre, m.descripcion, m.categoriaid,m.estadoid], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }
  module.exports = { obtenerProductosAlmacen,ModificarProductosAlmacen};
