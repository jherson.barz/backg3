const { obtenerConexion } = require('../database copy/conexion_mysql');
 
function obtenerVentas(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarVentas()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 


function ModificarVentas(m, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL insertarVenta(?, ?, ?,?,?, @parametro_salida)';
      connection.query(query, [m.idVentas,m.idProducto, m.cantidad, m.fecha, m.total], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }
  module.exports = { obtenerVentas,ModificarVentas};
