const { obtenerConexion } = require('../database copy/conexion_mysql');
 
function obtenerPrecio(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarPrecios()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 


function ModificarPrecio(m, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL insertarActualizacionPrecio(?, ?, ?,?, @parametro_salida)';
      connection.query(query, [m.id, m.idProducto, m.precio, m.fecha], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }
  module.exports = { obtenerPrecio,ModificarPrecio};
