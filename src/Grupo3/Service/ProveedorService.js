const { obtenerConexion } = require('../database copy/conexion_mysql');
 
function obtenerProveedor(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call Listarproveedores()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 


function ModificarProveedor(m, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL insertarActualizacionProveedores(?, ?, ?, @parametro_salida)';
      connection.query(query, [m.idProveedor, m.nombre, m.contacto], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }
  module.exports = { obtenerProveedor,ModificarProveedor};
