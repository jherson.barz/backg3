const {obtenerDetalleCompra,ModificarDetalleCompra} = require('../Service/DetalleCompraService')
 
function obtenerDetalleCompraController(req, res) {
  obtenerDetalleCompra((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function ModificarDetalleCompraController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarDetalleCompra(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerDetalleCompraController,ModificarDetalleCompraController}