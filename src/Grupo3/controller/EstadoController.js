const {ModificarEstado,obtenerEstado} = require('../Service/EstadoService')
 
function obtenerEstadoController(req, res) {
  obtenerEstado((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function ModificarEstadoController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarEstado(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerEstadoController,ModificarEstadoController}