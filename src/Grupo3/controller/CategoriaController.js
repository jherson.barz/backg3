const {obtenerCategoria,ModificarCategoria} = require('../Service/CategoriaService')
 
function obtenerCategoriaController(req, res) {
  obtenerCategoria((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function ModificarCategoriaController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarCategoria(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerCategoriaController,ModificarCategoriaController}