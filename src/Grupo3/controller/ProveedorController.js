const {obtenerProveedor,ModificarProveedor} = require('../Service/ProveedorService')
 
function ObtenerProveedorController(req, res) {
  obtenerProveedor((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function ModificarProveedorController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarProveedor(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={ObtenerProveedorController,ModificarProveedorController}