const {obtenerVentas,ModificarVentas} = require('../Service/VentasService')
 
function obtenerVentasController(req, res) {
    obtenerVentas((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function modificarVentasController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarVentas(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerVentasController,modificarVentasController}