const {obtenerStock,ModificarStock} = require('../Service/StockService')
 
function obtenerStockController(req, res) {
    obtenerStock((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function modificarStockController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarStock(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerStockController,modificarStockController}