const {obtenerProductosAlmacen,ModificarProductosAlmacen} = require('../Service/ProductosAlmacenService')
 
function obtenerProductosAlmacenController(req, res) {
  obtenerProductosAlmacen((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function modificarProductosAlmacenController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarProductosAlmacen(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerProductosAlmacenController,modificarProductosAlmacenController}