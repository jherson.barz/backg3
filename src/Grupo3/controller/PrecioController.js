const {obtenerPrecio,ModificarPrecio} = require('../Service/PrecioService')
 
function obtenerPrecioController(req, res) {
  obtenerPrecio((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
function ModificarPrecioController(req, res) {
    const parametrosEntrada = req.body;
 
    ModificarPrecio(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
module.exports={obtenerPrecioController,ModificarPrecioController}