const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',  // Cambia esto por el host de tu base de datos
  user: 'Rogers',    // Cambia esto por el nombre de usuario de tu base de datos
  password: 'RiasGremory', // Cambia esto por la contraseña de tu base de datos
  database: 'restaurante' // Cambia esto por el nombre de tu base de datos

});

function obtenerConexion(callback) {
  pool.getConnection((err, connection) => {
    if (err) {
      console.error('Error al obtener conexión del pool:', err);
      return callback(err);
    }
    console.log('Conexión obtenida del pool');
    callback(null, connection);
  });
}

module.exports = { obtenerConexion };
