const { obtenerConexion } = require('../database/conexion_mysql');
 
function obtenerPlatos(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('CALL ListarPlatos()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function modificarPlatos(p, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL sp_InsertarActualizaPlato(?, ?, ?, @parametro_salida)';
      connection.query(query, [p.ID_Plato , p.Nombre_Plato, p.Precio], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

module.exports = { obtenerPlatos, modificarPlatos };