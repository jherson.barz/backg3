const { obtenerConexion } = require('../database/conexion_mysql');
 
function obtenerCategoriaPlatos(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('CALL ListarCategoriaPlato()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function modificarCategoriaPlatos(cp, callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }

    const query = 'CALL sp_InsertarActualizarCategoriaPlato(?, ?, @parametro_salida)';
    connection.query(query, [cp.ID_Categoria_Plato , cp.Descripcion_Categoria], (err) => {
      if (err) {
        connection.release();
        return callback(err);
      }

      connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
        connection.release();
        if (err) {
          return callback(err);
        }

        const parametroSalida = results[0].parametro_salida;
        callback(null, { parametroSalida });
      });
    });
  });
}

module.exports = { obtenerCategoriaPlatos, modificarCategoriaPlatos };