// services/elementosService.js
 
const { obtenerConexion } = require('../database/ConexionMYSQL');
 
function obtenerDetalleComprobante(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('CALL listarDetalleComprobante()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function modificarComprobante(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL registrarOActualizarDetalleComprobante(?, ?, ?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.p_ID, parametrosEntrada.p_Descripcion, parametrosEntrada.p_Precio_Unitario, parametrosEntrada.p_Cantidad], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

module.exports = { obtenerDetalleComprobante, modificarComprobante};