const { obtenerCategoriaPlatos, modificarCategoriaPlatos} = require('../services/CategoriaPlatoService');
 
function obtenerCategoriaPlatosController(req, res) {
    obtenerCategoriaPlatos((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarCategoriaPlatosController(req, res) {
  const parametrosEntrada = req.body;

  modificarCategoriaPlatos(parametrosEntrada, (err, resultados) => {
    if (err) {
      console.error('Error al ejecutar el procedimiento:', err);
      return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
    }
    res.status(200).json(resultados);
  });
}

module.exports = { obtenerCategoriaPlatosController, modificarCategoriaPlatosController };