const { obtenerDetalleComprobante,modificarComprobante } = require('../services/DetalleComprobanteService');
 
function obtenerDetalleComprobanteController(req, res) {
    obtenerDetalleComprobante((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
    });
}

function modificarComprobanteController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarComprobante(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
 
module.exports = { obtenerDetalleComprobanteController, modificarComprobanteController };