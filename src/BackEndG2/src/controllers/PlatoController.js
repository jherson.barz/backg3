const { obtenerPlatos, modificarPlatos } = require('../services/PlatoService');
 
function obtenerPlatosController(req, res) {
    obtenerPlatos((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}
 
function modificarPlatosController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarPlatos(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }
 
module.exports = { obtenerPlatosController, modificarPlatosController };