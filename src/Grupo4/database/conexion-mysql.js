
const mysql = require('mysql');
 
const pool = mysql.createPool({
    connectionLimit: 10,
    host: '127.0.0.1',
    user: 'Alvaro25',
    password: 'alvaro25',
    database: 'restaurante'
 
});
 
function obtenerConexion(callback) {
  pool.getConnection((err, connection) => {
    if (err) {
      console.error('Error al obtener conexión del pool:', err);
      return callback(err);
    }
    console.log('Conexión obtenida del pool');
    callback(null, connection);
  });
}
 
module.exports = { obtenerConexion };