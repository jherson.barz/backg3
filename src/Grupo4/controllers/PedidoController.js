const {obtenerPedidos,modificarPedido} = require('../services/PedidoService')
 
function obtenerPedidosController(req, res) {
    obtenerPedidos((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarPedidoController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarPedido(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


module.exports = {obtenerPedidosController,modificarPedidoController};