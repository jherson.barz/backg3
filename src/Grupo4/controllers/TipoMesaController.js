const {obtenerTipoMesa,modificarTipoMesa} = require('../services/TipoMesaService')
 
function obtenerTipoMesaController(req, res) {
    obtenerTipoMesa((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarTipoMesaController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarTipoMesa(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


module.exports = {obtenerTipoMesaController,modificarTipoMesaController};