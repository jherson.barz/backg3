const {obtenerTipoUsuario,modificarTipoUsuario} = require('../services/TipoUsuarioService')
 
function obtenerTipoUsuarioController(req, res) {
    obtenerTipoUsuario((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarTipoUsuarioController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarTipoUsuario(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


module.exports = {obtenerTipoUsuarioController,modificarTipoUsuarioController};