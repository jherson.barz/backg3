const {obtenerUsuario,modificarUsuario} = require('../services/UsuarioService')
 
function obtenerUsuarioController(req, res) {
    obtenerUsuario((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarUsuarioController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarUsuario(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


module.exports = {obtenerUsuarioController,modificarUsuarioController};