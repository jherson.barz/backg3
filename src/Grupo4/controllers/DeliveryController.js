const {obtenerDelivery,modificarDelivery} = require('../services/DeliveryService')
 
function obtenerDeliveryController(req, res) {
    obtenerDelivery((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarDeliveryController(req, res) {
    const parametrosEntrada = req.body;
 
    modificarDelivery(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


module.exports = {obtenerDeliveryController,modificarDeliveryController};