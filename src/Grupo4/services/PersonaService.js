const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerPersonas(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarPersonas()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarPersona(c, callback) {
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
   
        const query = 'CALL CrearActualizarPersona(?, ?, ?, ?,?, @parametro_salida)';
        connection.query(query, [c.idpersona, c.nombres, c.apellidos, c.dni, c.direccion], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
   
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
   
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
    }

module.exports = { obtenerPersonas,modificarPersona};