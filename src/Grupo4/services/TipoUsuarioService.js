const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerTipoUsuario(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarTipoUsuarios()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarTipoUsuario(tpu, callback) {
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
   
        const query = 'CALL CrearActualizarTipoUsuario(?, ?, @parametro_salida)';
        connection.query(query, [tpu.tipousuarioid, tpu.descripcion], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
   
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
   
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
    }

module.exports = { obtenerTipoUsuario,modificarTipoUsuario};