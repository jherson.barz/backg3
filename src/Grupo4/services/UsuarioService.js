const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerUsuario(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarUsuarios()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarUsuario(us, callback) {
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
   
        const query = 'CALL CrearActualizarUsuario(?, ?, ?, ?, @parametro_salida)';
        connection.query(query, [us.idusuario, us.idpersona, us.tipousuarioid, us.estado], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
   
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
   
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
    }

module.exports = { obtenerUsuario,modificarUsuario};