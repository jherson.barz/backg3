const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerPedidos(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarPedido()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarPedido(pe, callback) {
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
   
        const query = 'CALL CrearActualizarPedido(?, ?, ?, ?, ?, ?, @parametro_salida)';
        connection.query(query, [pe.idpedido, pe.idplato, pe.idadicional, pe.idusuario, pe.idmesa, pe.estado], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
   
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
   
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
    }

module.exports = { obtenerPedidos,modificarPedido};