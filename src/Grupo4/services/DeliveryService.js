const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerDelivery(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarDeliverys()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarDelivery(d, callback) {
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
   
        const query = 'CALL CrearActualizarDelivery(?, ?, ?, ?, ?, ?, @parametro_salida)';
        connection.query(query, [d.iddelivery, d.idpedido, d.fecha_entrega, d.direccion_entrega, d.idrepartidor, d.estado], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
   
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
   
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
    }

module.exports = { obtenerDelivery,modificarDelivery};