const { obtenerConexion } = require('../database/conexion_Mysql');
 
function obtenerReservas(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('CALL ListarReservas()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarReserva(r, callback) {
  obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL CrearActualizarReserva(?, ?, ?, ?, ?, ?, ?, @parametro_salida)';
      connection.query(query, [r.idReserva, r.FechaR, r.nroPersonas, r.estado, r.nomCliente,
                                r.telfCliente, r.email], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  module.exports = { obtenerReservas,modificarReserva};