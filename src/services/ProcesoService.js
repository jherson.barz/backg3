const { obtenerConexion } = require('../database/conexion_Mysql');
 
function obtenerProcesos(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('CALL ListarProcesos()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarProceso(pro, callback) {
  obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL CrearActualizarProceso(?, ?, ?, ?, ?, @parametro_salida)';
      connection.query(query, [pro.idProceso, pro.descripcion, pro.fechaI, pro.fechaf, pro.estado], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  module.exports = { obtenerProcesos,modificarProceso};