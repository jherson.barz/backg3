const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerEmpleados(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarEmpleado()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarEmpleado(e, callback) {
  obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL CrearActualizarEmpleado(?, ?, ?, ?, ?, ?, ?, @parametro_salida)';
      connection.query(query, [e.idEmpleado, e.nombre, e.apellido, e.fechaNacimiento, e.genero,
                                    e.direccion, e.telefono], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  module.exports = { obtenerEmpleados,modificarEmpleado};