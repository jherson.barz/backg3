const { obtenerConexion } = require('../database/conexion-mysql');
 
function obtenerCargos(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }
 
    connection.query('call ListarCargo()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
} 

function modificarCargo(c, callback) {
  obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
 
      const query = 'CALL CrearActualizarCargo(?, ?, ?, @parametro_salida)';
      connection.query(query, [c.idCargo, c.nombre, c.descripcion], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
 
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
 
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  module.exports = { obtenerCargos,modificarCargo};
 
